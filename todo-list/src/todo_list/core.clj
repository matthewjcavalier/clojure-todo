(ns todo-list.core
  (:require [ring.adapter.jetty :as webserver]))

(defn -main
  "A very simple web server built with Ring and Jetty"
  [port-number]
  (webserver/run-jetty
   (fn [request]
     {:status 200
      :headers {}
      :body "<h1>Hello Clojure World!</h1>
             <p>Baby's first clojure app. This message doesn't care what you sent
to the server :P </p>"})
   {:port (Integer. port-number)
    :join? false}))


